/**
 * Poll maker - make a poll using Google Forms from a text in Google Docs
 * @copyright Google Inc., December 22nd 2015
 * @author Javier Cañadillas (javiercm@google.com)
 * @version 1.1
 * @tutorial <Insert tutorial URL here.>
 * Add-on requires to access Drive to create a Form, so
 * @NotOnlyCurrentDoc
 */

/**
 * Creates a menu entry in the Google Docs UI when the document is opened.
 */
 function onOpen() {
  'use strict';
  DocumentApp.getUi().createAddonMenu()
  .addItem(language[userLang].Menu_process, 'processSlogans')
  .addItem(language[userLang].Menu_config, 'configureSettings')
  .addToUi();
}

/**
 * Runs when the add-on is installed.
 */
 function onInstall(e) {
  'use strict';
  onOpen(e);
}

/**
 * Opens a sidebar in the document containing the add-on's main interface.
 */
 function processSlogans() {
  'use strict';
  var title = language[userLang].Menu_process;
  var ui = HtmlService.createTemplateFromFile('Sidebar');
  for (var prop in language[userLang].Sidebar) {
    if (language[userLang].Sidebar.hasOwnProperty(prop)) {
      ui[prop] = language[userLang].Sidebar[prop];
      Logger.log(ui[prop]);
    }
  }
  DocumentApp.getUi().showSidebar(ui.evaluate()
    .setSandboxMode(HtmlService.SandboxMode.IFRAME).setTitle(title));
}

/**
 * Opens a sidebar in the document containing the add-on's settings interface.
 */
 function configureSettings() {
  'use strict';
  var title = language[userLang].Menu_config;
  var ui = HtmlService.createTemplateFromFile('Settings');
  // Assign language settings object properties to ui for template substitution
  for (var prop in language[userLang].Settings) {
    if (language[userLang].Settings.hasOwnProperty(prop)) {
      ui[prop] = language[userLang].Settings[prop];
      Logger.log(ui[prop]);
    }
  }
  DocumentApp.getUi().showSidebar(ui.evaluate()
    .setSandboxMode(HtmlService.SandboxMode.IFRAME).setTitle(title));
}

/**
 * Gets stored preferences from Script Properties.
 * @return  {object} The object storing preferences.
 */
 function getPreferences() {
  'use strict';
  var pollPreferences = PropertiesService.getDocumentProperties()
    .getProperties();
  return pollPreferences;
}

/**
 * Stores configuration object as Document Properties attributes.
 * @param   {object} pollPreferences The preferences object.
 * @return  {string} Result message.
 */
 function savePreferences(pollPreferences) {
  'use strict'; 
  // Test if required settins are set
  testPreferenceDeps(
    'To:',
    pollPreferences.selectMail,
    pollPreferences.mailTo);
  testPreferenceDeps(
    'Form ID:',
    pollPreferences.selectForm,
    pollPreferences.formID);
  try {
    PropertiesService.getDocumentProperties()
      .setProperties(pollPreferences, true);
    return language[userLang].SavePrefs_msgOk;
  } catch(e) {
    Logger.log('Error %s saving properties', e);
    return language[userLang].SavePrefs_msgNOk;
  }
}

/**
 * Tests dependencies between a parent checkbox preference and an additional
 * info preference
 * @param   {string} name       Required (additional) configuration field name.
 * @param   {string} checked    Check-style configuration setting that enables
 *                              the appearance of an additional (and required)
 *                              sub-configuration setting.
 *                              For example, if an existing Form is going to be
 *                              used (selectForm is checked), then it's required
 *                              to fill in an additional configuration setting
 *                              called Form ID.
 * @param   {string} additional The additional (and required) configuration
 *                              setting to be filled.
 * @return  {object}            The object storing preferences
 */
 function testPreferenceDeps(name, checked, additional) {
  'use strict';
  if (checked) {
    if (!additional) {
      var errorMsg = language[userLang].TestPrefs1 + ' ' + name + ' '
        + language[userLang].TestPrefs2 + '.';
      throw errorMsg;
    }
  }
}

/**
 * Gets slogans from current document in a different fashion depending on
 * the kind of document (enabled via a document property). If it's a Google
 * standard immersive agenda, a function will be called to extract text
 * from a table that's expected to be there. If not, another function that
 * extracts selected text in the document will be called.
 * @return {array}  Slogans text.
 */
 function getSlogans() {
  'use strict';
  // Based on selectImm property, get them from table or from selected text.
  var selectImm = PropertiesService.getDocumentProperties()
    .getProperty('selectImm');
  if (selectImm === 'true') {
    return getTextfromImmTable();
  } else {
    return getSelectedText();
  }

}

/**
 * Gets the text the user has selected. If there is no selection,
 * this function displays an error message.
 * @return {array}  The selected text.
 */
 function getSelectedText() {
  'use strict';
  var selection = DocumentApp.getActiveDocument().getSelection();
  if (selection) {
    var text = [];
    var elements = selection.getSelectedElements();
    for (var i = 0; i < elements.length; i++) {
      var element;
      if (elements[i].isPartial()) {
        element = elements[i].getElement().asText();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();

        text.push(element.getText().substring(startIndex, endIndex + 1));
      } else {
        element = elements[i].getElement();
        // Only get elements that can be edited as text; skip images and
        // other non-text elements.
        if (element.editAsText) {
          var elementText = element.asText().getText();
          // This check is necessary to exclude images, which return a blank
          // text element.
          if (elementText !== '') {
            text.push(elementText);
          }
        }
      }
    }
    if (text.length === 0) {
      throw 'Please select some text.';
    }
    return text; // Returns an array 
  } else {
    throw 'Please select some text.';
  }
}

/**
 * Extracts text from the first table in the document, ignoring the first line
 * present in each cell.
 * @return {array}  The text contained in the table cells, line breaks  
 *                  considered. 
 */
 function getTextfromImmTable() {
  'use strict';
  var docTables = DocumentApp.getActiveDocument().getBody().getTables();
  var firstTable = docTables[0];
  var numRows = firstTable.getNumRows();
  var fullTextArray = [];
  for (var i=0; i < numRows; i++) {
    var currentRow = firstTable.getRow(i);
    var rowNumCells = currentRow.getNumCells();
    for (var j=0; j < rowNumCells; j++) {
      var currentCell= currentRow.getCell(j);
      var currentCellText = currentCell.getText();
      var currentCellTextArray = currentCellText.match(/[^\r\n]+/g);
      currentCellTextArray.shift();
      var trimmedCellTextArray = currentCellTextArray.map(function(s) {
        return s.trim();});
      fullTextArray = fullTextArray.concat(trimmedCellTextArray);
    }
  }
  // @todo: test if this works the same returning an array
  //   (See getSelectedText() return type)
  var fullText = fullTextArray.join('\n');
  return fullText;
}

/**
 * Creates a new list item in either a new or preexisting Form containing
 * the list of slogans extracted fromm the document. The choice of a new or
 * existing form is taken based on the pollPreferences.selectForm property
 * value. It will also create and display a QR, and send a mail notification
 * with a link to the form based on the value of pollPreferences.selectQR and
 * pollPreferences.selectMail as preferences markers for them.
 * @param   {array}   slogansText A text array contaning the slogans
 * @return  {string}  Returns either nothing or an error message
 *                    after failing to perform any of the form
 *                    opening | creation | mail sending.
 */
 function createPoll(slogansText) {
  'use strict';
  var pollPreferences = PropertiesService.getDocumentProperties()
    .getProperties();
  var formLang = pollPreferences.formLanguage;
  var mailLang = pollPreferences.mailLanguage;
  var slogansArray = slogansText.match(/[^\r\n]+/g);
  var votingForm;
  if (pollPreferences.selectForm === 'true') {
    try {
      votingForm = FormApp.openById(pollPreferences.formID);
    } catch(e) {
      return e;
    }
  } else {
    try {
      // Create the form (localization included)
      votingForm = FormApp.create(language[formLang].Form_Title);
    } catch(e) {
      return e;
    }
  }
  // Set voting form preferences
  votingForm.setRequireLogin(false);
  // Add slogans to the form (localization included)
  var slogansList = votingForm.addListItem();
  slogansList.setTitle(language[formLang].Form_QuestionTitle)
    .setChoiceValues(slogansArray);
  var formURL = votingForm.getPublishedUrl();
  // Test for QR selection
  if (pollPreferences.selectQR === 'true') {
    var qrImageURL = make_QR(formURL);
    displayImage(qrImageURL);
  }
  // Test for mail notification preferences
  if (pollPreferences.selectMail === 'true') {
    var recipients = pollPreferences.mailTo;
    var subject = language[mailLang].Mail_Subject;
    var htmlBody = HtmlService
                .createHtmlOutputFromFile(language[mailLang].Mail_Template)
                .getContent().replace('%%formURL%%',formURL);
    try {
      GmailApp.sendEmail(recipients, subject,'',{htmlBody:htmlBody});
    } catch(e) {
      return e;
    }
  }
  // Insert a paragraph with the form link in the current doc
  var docBody = DocumentApp.getActiveDocument().getBody();
  docBody.appendParagraph(language[formLang].Doc_FormLink)
    .setLinkUrl(formURL);
}

/**
 * Displays a image in an HTML dialog given its URL
 * @param {string} imageURL The image URL to be displayed.
 */
 function displayImage(imageURL) {
  'use strict';
  var t = HtmlService.createTemplateFromFile('QRUI');
  t.imageURL = imageURL;
  t.QR_Close = language[userLang].QR_Close;
  var html = t.evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME)
  .setWidth(410)
  .setHeight(450);
  DocumentApp.getUi().showModalDialog(html, language[userLang].QR_Message);
}

/**
 * Generates a QR code for a given URL
 * @param  {string} url URL to generate the image from.
 * @return {string} image_url URL to the QR image hosted in Google Charts
 */
 function make_QR(url) {
  'use strict';
  var size = 400;
  var encoded_url = encodeURIComponent(url);
  var image_url = 'http://chart.googleapis.com/chart?chs=' + 
                  size + 'x' + size + '&cht=qr&chl=' + encoded_url;
  return image_url;
}

/**
 * Get languages object
 * @return {array} List of supported languages for mail notification
 */
 function getLangs () {
  'use strict';
  var langList = [];
  for (var key in language) {
    if (language.hasOwnProperty(key)) {
      langList.push(key);
    }
  }
  return langList;
}

/**
 * Helper function that puts external JS/CSS into the HTML file
 * @param  {string} filename File name without extension
 * @return {object}          Content to be included
 */
 function include(filename) {
  'use strict';
  return HtmlService.createHtmlOutputFromFile(filename)
    .getContent();
}
