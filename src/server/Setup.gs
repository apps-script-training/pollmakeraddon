'use strict';
var supportedLangs = ['EN','ES'];
var userLang = setUserLang();
Logger.log(userLang);

function setUserLang() {
  Logger.log(supportedLangs);
  var currentLocale = Session.getActiveUserLocale().toUpperCase();
  var userLang = (supportedLangs.indexOf(currentLocale) != -1) ? 'EN' : currentLocale ;
  Logger.log('Selected user locale is %s',userLang);
  return userLang;
};

var language = {
	EN: {
		description: 'English',
		loc_langs: ['English','Spanish'],
		Menu_process: 'Process Slogans',
		Menu_config: 'Configure Settings',
		SavePrefs_msgOk: 'Settings Saved',
		SavePrefs_msgNOk: 'Error saving settings',
		Settings: {
			MailLangOpt: 'Mail Language',
			ImmOpt: 'Immersive Mode',
			QROpt: 'Generate QR code',
			MailOpt: 'Send by mail',
			MailTo: 'To:',
			FormOpt: 'Use existing Form',
			FormID: 'Form ID',
			FormLangOpt: 'Form Language',
			SaveButton: 'Save Settings',
		},
		Sidebar: {
			PollWindow: 'These are the candidates so far:',
			GetSlogans: 'Get slogans',
			CreatePoll: 'Create Poll',
		},
		Form_Title: 'Vote our new campaign slogan!',
		Form_QuestionTitle: 'Choose your preferred option',
		Mail_Subject: 'Deciding on a campaign slogan',
		Mail_Template: 'Mailbody_EN',
		Doc_FormLink: 'Click here to vote',
		TestPrefs1: 'Field',
		TestPrefs2: 'cannot be empty',
		QR_Message: 'Scan the QR code to access the poll',
		QR_Close: 'Close'
	},
	ES: {
		description: 'Spanish',
		loc_langs: ['Inglés','Español'],
		Menu_process: 'Procesar lemas',
		Menu_config: 'Configurar',
		SavePrefs_msgOk: 'Preferencias guardadas',
		SavePrefs_msgNOk: 'Error al guardar',
		Settings: {
			MailLangOpt: 'Idioma del correo',
			ImmOpt: 'Modo Immersivo',
			QROpt: 'Generar código QR',
			MailOpt: 'Enviar por correo',
			MailTo: 'A:',
			FormOpt: 'Usar formulario previo',
			FormID: 'Form ID',
			FormLangOpt: 'Idioma del formulario',
			SaveButton: 'Guardar cambios',
		},
		Sidebar: {
			PollWindow: 'Esto son los lemas candidatos:',
			GetSlogans: 'Capturar slogans',
			CreatePoll: 'Crear votación',
		},
		Form_Title: 'Vota el nuevo lema de la campaña',
		Form_QuestionTitle: 'Elige tu opción preferida',
		Mail_Subject: 'Decidiendo el lema de la campaña',
		Mail_Template: 'Mailbody_ES',
		Doc_FormLink: 'Pulsa aquí para votar',
		TestPrefs1: 'El campo',
		TestPrefs2: 'no puede estar vacío',
		QR_Message: 'Escanea el código para votar',
		QR_Close: 'Cerrar'
	}
};